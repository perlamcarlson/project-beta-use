import React from "react";

function VehicleModelsTable(props) {
    return (
        <tr key={props.model.id}>
        <td> {props.model.name}</td>
        <td>{props.model.manufacturer.name}</td>
    <img src={props.model.picture_url}/>
        </tr>
    )
}

class VehicleModelsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            models: [],

        };
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/models/";
        const response =await fetch (url);
        const data = await response.json();

        if (response.ok) {
            console.log(data)
            this.setState({ models: data.models});
        }
    }

    render() {
        return (

            <>
            <div className="px-4 py-5 my-5">
                <h1 className="display-4 fw-bold text-center">Models</h1>
                <table className="table .table-bordered table-striped table-success text-left">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.state.models.map((model) => {
                                return (
                                    <VehicleModelsTable
                                    model={model}
                                    key={model.id}
                                    
                                />
                                );
                            })}
                        </tbody>
                </table>
            </div>
            </>
                    );
    }
}
export default VehicleModelsList;