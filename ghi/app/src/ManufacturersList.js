import React from "react";


function ManufacturerTable(props) {
    return (
        <tr key={props.manufacturer.id}>
            <td>{props.manufacturer.name}</td>
        </tr>
    )
}

class ManufacturerList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            manufacturers: [],
        };
    }


    async componentDidMount() {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch (url);
        const data = await response.json();

        if (response.ok) {
            console.log(data)
            this.setState({ manufacturers: data.manufacturers});
        }
}   
    render() {
        return (
            <>
            <div className="px-4 py-5 my-5">
                <h1 className="display-4 fw-bold text-center">Manufacturers</h1>
                <table className="table .table-bordered table-striped table-success text-left">
                <thead>
                        <tr>
                        <th >Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map((manufacturer) => {
                            return (
                                <ManufacturerTable
                                manufacturer={manufacturer}
                                key={manufacturer.id}
                               
                                />
                            );
                        })}
                    </tbody>
                </table>
            </div>
            </>
        );
    }
}
export default ManufacturerList;