from django.db import models
from django.urls import reverse


# Create your models here.


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    vip = models.BooleanField(default=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

    def __str__(self):
        return self.name


class Status(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "statuses"  # Fix the pluralization


class Appointment(models.Model):
    vin = models.ForeignKey(AutomobileVO, related_name="appointments", on_delete=models.CASCADE)
    owner = models.CharField(max_length=100)
    date = models.DateTimeField()
    time = models.TimeField()
    technician = models.ForeignKey(Technician, related_name="appointments", on_delete=models.CASCADE)
    reason = models.CharField(max_length=200)
    status = models.ForeignKey(Status, related_name="appointments", null=True, on_delete=models.CASCADE)

    def cancel(self):
        status = Status.objects.get(name="cancelled")
        self.status = status
        self.save()

    def finished(self):
        status = Status.objects.get(name="finished")
        self.status = status
        self.save()

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return self.owner

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="scheduled")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment
