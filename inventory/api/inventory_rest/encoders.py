from common.json import ModelEncoder

from .models import Automobile, Manufacturer, VehicleModel


class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = [
        "id",
        "name",
    ]

# this encoder will also include the href propety because
# Manufacturer model has a method of get_url_api which
# inside common.json.py file, the ModelEncoder there's code
# that says if model has "get_api_url", then return
# create the href key and value

class VehicleModelEncoder(ModelEncoder):
    model = VehicleModel
    properties = [
        "id",
        "name",
        "picture_url",
        "manufacturer",
    ]
    encoders = {
        "manufacturer": ManufacturerEncoder(),
    }
# need ManufacturerEncoder here because manufacturer
# is a foreignkey having multiple properties, so
# in order for VehicleModel to use those manufacture fields,
# it needs to be translated into JSON-friendly values

class AutomobileEncoder(ModelEncoder):
    model = Automobile
    properties = [
        "id",
        "color",
        "year",
        "vin",
        "model",
    ]
    encoders = {
        "model": VehicleModelEncoder(),
    }
